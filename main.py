import json
import twitter
from dateutil import parser, tz
from datetime import datetime
from pprint import pprint


api = twitter.Api(consumer_key='OuW9bsPpyX7Y6OBFc2oBgcniI',
                  consumer_secret='Uy69GVNeXRNODbzp3ANAT6PcJeB50orENof76ppMGK5LS1qhFi',
                  access_token_key='28704527-rynKvS9hJu3fj48VTiHF3QT2PE1yw0IytaXFxW4fs',
                  access_token_secret='BXoBKuQJdHEXYi7jtTSfrSy5NjOABlquYyiPBNd70z8mL')


def time_elapsed(start_date, end_date):
    date_diff = start_date - end_date
    if date_diff.days > 0:
        return "%s day" % date_diff.days
    else:
        minutes = date_diff.seconds / 60
        if minutes < 60:
            return "%sm" % minutes

        hours = minutes / 60
        return "%sh" % hours


def main():
    with open('tweets.json', 'r') as d:
        data = d.read()

    tweets = json.loads(data)
    for tweet in tweets:
        created = parser.parse(tweet["created_at"])
        posted_by = tweet["user"]["screen_name"]
        tags = [t["text"] for t in tweet["entities"]["hashtags"]]
        text = tweet["text"]
        now = datetime.now(tz.tzutc())
        elapsed = time_elapsed(now, created)

        # print elapsed, posted_by, tags, text
        print "%s - %s" % (posted_by, elapsed)
        print text
        if tags:
            print " ".join(tags)
        print "\n"

    # users = ["@Raspberry_Pi", "@CyberScimitar"]
    # screen_names = ["@malwrhunterteam", "@Techhelplistcom", "@mesa_matt", "@JAMESWT_MHT", "@dvk01uk"]
    # users = []
    #
    # for nm in screen_names:
    #     user = api.GetUser(screen_name=nm)
    #     users.append(str(user.id))
    #
    # for tweet in api.GetStreamFilter(follow=users):
    #     if tweet['in_reply_to_user_id'] is None:
    #         created = tweet["created_at"]
    #         posted_by = tweet["user"]["screen_name"]
    #         tags = tweet["entities"]["hashtags"]
    #         text = tweet["text"]
    #         pprint(tweet)

    # users = api.GetUserTimeline(screen_name="@Raspberry_Pi", include_rts=False, exclude_replies=True)
    # pprint(users)


if __name__ == "__main__":
    main()